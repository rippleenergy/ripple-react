import React from 'react'
import {FormComponent } from "./FormComponent"
import {observer, useLocalStore} from "mobx-react-lite"
import {isObservableArray} from "mobx"


export function RippleValue(props) {
    return props.value || props.default || props.children || ''
}

class WiredHelper extends FormComponent {

    constructor(props) {
        super(props)
        if (props.chain) {
            this.root = props.chain
        }
        else {
            this.root = this
        }
        if (props.onSubmit) {
            this.onSubmit = props.onSubmit
        }

        if (props.onSuccess) {
            this.onSuccess = props.onSuccess
        }

        if (props.serverError) {
            this.serverError = props.serverError
        }

        if (props.onChange) {
            this.onChange = props.onChange
        }

    }


    static getProp(obj, desc, tp) {
        let arr = desc.split('.')
        while (arr.length) {
            obj = obj[arr.shift()]
        }
        if (tp === 'dt' && obj) {
            obj = new Date(obj).toLocaleString()
        }
        if (tp === 'date' && obj) {
            obj = new Date(obj).toLocaleDateString()
        }
        return obj
    }

    static setProp(obj, desc, value) {
      let arr = desc.split('.')
      while (arr.length-1) {
          obj = obj[arr.shift()]
      }
      obj[arr[0]] = value
    }

    static replace_variables(s, v, ns) {
        s = s.replace(new RegExp(`\\$\\((\\[[^\\]]*\\])?${ns}([a-zA-Z0-9_.]*)([|:][^)]*)?\\)`,'g'), (m,t,a,b) => {
            if (t) t = t.slice(1,-1)
            try {
                const ret = this.getProp(v, a, t)
                if (ret) return ret
                if (ret === '') {
                    return (!b || b[0] === ':' )?'':b.slice(1)
                }
            }
            catch {}
            return b?b.slice(1):m
        })
        return s
    }

    static cloneChildren(children, values, add_prop, replace_child) {
        return React.Children.map(children, child => {
            const rep = replace_child && replace_child(child, values)
            if (rep || rep === '') return rep
            if (child && child.props) {
                const props = add_prop(child, values)
                if (child.props.children) {
                    return React.cloneElement(child, props || {},
                        ...this.cloneChildren(child.props.children, values, add_prop, replace_child))
                }
                if (props) {
                    return React.cloneElement(child, props)
                }
            }
            return child
        })
    }

    add_prop = (child, values) => {

        let ret = {}

        const ns = this.props.namespace?`${this.props.namespace}.`:''

        if (values) {
            Object.keys(child.props).forEach( (k) => {
                if (typeof(child.props[k]) === 'string') {
                    ret[k] = this.constructor.replace_variables(child.props[k], values, ns)
                }
        })
        }

        if (child.props.ripplefield && (ns==='' || this.props.namespace===child.props.namespace) ) {

            ret.onChange = this.root.handleChange
            try {
                ret.defaultValue = this.constructor.getProp(values, child.props.name)
            } catch {}
        }

        if (child.props.type==="input" || child.props.type==="email") {
            ret.onChange = this.root.handleChange
            try {
                const v =  this.constructor.getProp(values, child.props.name)
                if (v) {
                    ret.defaultValue = v
                }
                else {
                    if (child.props.defaultValue) this.constructor.setProp(values, child.props.name, child.props.defaultValue)
                }
            } catch {}
        }
        if (child.props.type==="radio" || child.props.type==="checkbox") {
            ret.onChange = this.root.handleChange
            try {
                let v = this.constructor.getProp(values, child.props.name)
                if (v) {
                    ret.defaultChecked = v === child.props.value
                }
                else {
                    if (child.props.defaultChecked) this.constructor.setProp(values, child.props.name, child.props.value)
                }
            } catch {}
        }
        if (child.props.onSubmit) {
            ret.onSubmit = this.root.submit
        }

        if (child.props.chain) {
            ret.chain = this.root
        }

        return ret
    }

    replace_child = (child, values) => {
        if (!child) return
        const ns = this.props.namespace?`${this.props.namespace}.`:''
        if (values && typeof child === 'string') {
            return this.constructor.replace_variables(child, values, ns)
        }

        if (child.type && (child.type.name === 'RippleValue') && (!child.props.namespace || child.props.namespace === this.props.namespace) ) {
                try {
                    return this.constructor.getProp(values, child.props.name)
                }
                catch {}
        }
    }
}


export const RippleWired = observer(function RippleWired(props) {

    const helper = useLocalStore(() => new WiredHelper(props))

    if (props.children) {
        if (props.list || isObservableArray(helper.root.values) ) {
            if ( isObservableArray(helper.root.values) && helper.root.values.length>0 ) {
                return (
                    <React.Fragment>
                        {
                             helper.root.values.map((v) => {
                                return helper.constructor.cloneChildren(props.children, v, helper.add_prop, helper.replace_child)
                            })
                        }
                    </React.Fragment>
                )
            }
            return (
                props.empty || ''
            )

        } else {
            return (
                <React.Fragment>
                    {helper.constructor.cloneChildren(props.children, helper.root.values, helper.add_prop, helper.replace_child)}
                </React.Fragment>
            )
        }
    }

    if (helper.root !== this) return

    let  ch = []
    for (let a in helper.values) {
        ch.push(<div key={a}>{a}: {helper.values[a]}</div>)
    }

    return (
        <div>
            { ch }
        </div>
    )
})