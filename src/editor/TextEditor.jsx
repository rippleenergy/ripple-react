import React from "react"
import CKEditor from '@ckeditor/ckeditor5-react'
import InlineEditor from '@ckeditor/ckeditor5-editor-inline/src/inlineeditor'

import cookie from "react-cookies"
import reactElementToJSXString from 'react-element-to-jsx-string'

import Essentials from '@ckeditor/ckeditor5-essentials/src/essentials';
import Autoformat from '@ckeditor/ckeditor5-autoformat/src/autoformat';
import BlockQuote from '@ckeditor/ckeditor5-block-quote/src/blockquote';
import Bold from '@ckeditor/ckeditor5-basic-styles/src/bold';
import Heading from '@ckeditor/ckeditor5-heading/src/heading';
import Image from '@ckeditor/ckeditor5-image/src/image';
import ImageCaption from '@ckeditor/ckeditor5-image/src/imagecaption';
import ImageStyle from '@ckeditor/ckeditor5-image/src/imagestyle';
import ImageToolbar from '@ckeditor/ckeditor5-image/src/imagetoolbar';
import ImageUpload from '@ckeditor/ckeditor5-image/src/imageupload'
import Italic from '@ckeditor/ckeditor5-basic-styles/src/italic';
import Link from '@ckeditor/ckeditor5-link/src/link';
import List from '@ckeditor/ckeditor5-list/src/list';
import MediaEmbed from '@ckeditor/ckeditor5-media-embed/src/mediaembed'
import Table from '@ckeditor/ckeditor5-table/src/table';
import TableToolbar from '@ckeditor/ckeditor5-table/src/tabletoolbar';
import Alignment from '@ckeditor/ckeditor5-alignment/src/alignment'
import BallonToolbar from '@ckeditor/ckeditor5-ui/src/toolbar/balloon/balloontoolbar'
import Paragraph from '@ckeditor/ckeditor5-paragraph/src/paragraph'
import FileRepository from '@ckeditor/ckeditor5-upload/src/filerepository'


import ButtonView from '@ckeditor/ckeditor5-ui/src/button/buttonview'
import imageIcon from '@ckeditor/ckeditor5-core/theme/icons/image.svg'

import './ckeditor.css'

class MyUploadAdapter {
    constructor( loader ) {
        // The file loader instance to use during the upload.
        this.loader = loader
    }

    // Starts the upload process.
    upload() {
        return new Promise( ( resolve, reject ) => {
            this._initRequest()
            this._initListeners( resolve, reject )
            this._sendRequest()
        } )
    }

    // Aborts the upload process.
    abort() {
        if ( this.xhr ) {
            this.xhr.abort()
        }
    }

    // Initializes the XMLHttpRequest object using the URL passed to the constructor.
    _initRequest() {
        const xhr = this.xhr = new XMLHttpRequest()
        xhr.open( 'POST', '/api/upload', true )
        xhr.responseType = 'json'
    }

    // Initializes XMLHttpRequest listeners.
    _initListeners( resolve, reject ) {
        const xhr = this.xhr
        const loader = this.loader
        const genericErrorText = 'Couldn\'t upload file:' + ` ${ loader.file.name }.`

        xhr.addEventListener( 'error', () => reject( genericErrorText ) )
        xhr.addEventListener( 'abort', () => reject() )
        xhr.addEventListener( 'load', () => {
            const response = xhr.response

            if ( !response || response.error ) {
                return reject( response && response.error ? response.error.message : genericErrorText )
            }

            resolve( {
                default: response.url
            } )
        } )

        if ( xhr.upload ) {
            xhr.upload.addEventListener( 'progress', evt => {
                if ( evt.lengthComputable ) {
                    loader.uploadTotal = evt.total
                    loader.uploaded = evt.loaded
                }
            } )
        }
    }

    // Prepares the data and sends the request.
    async _sendRequest() {
        // Prepare the form data.
        const data = new FormData()
        const file = await this.loader.file
        data.append( 'upload', file )
        const csrftoken = cookie.load('csrftoken')
        if (csrftoken) {
            this.xhr.setRequestHeader("X-CSRFToken", csrftoken)
        }

        // Send the request.
        this.xhr.send( data )
    }
}

function MyUploadAdapterPlugin( editor ) {
    editor.plugins.get( 'FileRepository' ).createUploadAdapter = ( loader ) => {
        // Configure the URL to the upload script in your back-end here!
        return new MyUploadAdapter( loader )
    }
}

class PublishArticle {

    editor = null

    constructor( editor ) {
        this.editor = editor
    }

    init() {
        const editor = this.editor;

        editor.ui.componentFactory.add( 'publishArticle', locale => {
            const view = new ButtonView( locale );

            view.set( {
                label: 'Publish article',
                icon: imageIcon,
                tooltip: true
            } )

            // Callback executed once the image is clicked.
            view.on( 'execute', () => {
                if (confirm( 'Are you sure you want to publish this article for all?' )) {
                    editor.textasset.publish()
                }
            })
            return view
        } )
    }
}

const TextConfig = {
                    plugins: [
                        Essentials,
                        Autoformat,
                        Alignment,
                        BlockQuote,
                        Bold,
                        Heading,
                        Image,
                        ImageCaption,
                        ImageStyle,
                        ImageToolbar,
                        ImageUpload,
                        Italic,
                        Link,
                        List,
                        MediaEmbed,
                        Table,
                        TableToolbar,
                        FileRepository,
                        BallonToolbar,
                        PublishArticle,
                        MyUploadAdapterPlugin
                         ],
                    toolbar: {
                        items: [
                          'heading',
                          '|',
                          'alignment',
                          'bold',
                          'italic',
                          'link',
                          'bulletedList',
                          'numberedList',
                          'mediaEmbed',
                          'insertTable',
                          'imageUpload',
                          'blockQuote',
                          'publishArticle',
                          'undo',
                          'redo'
                        ]
                    },
                    image: {
                        toolbar: [
                            'imageStyle:full',
                            'imageStyle:side',
                            '|',
                            'imageTextAlternative'
                        ]
                    },
                    table: {
                        contentToolbar: [ 'tableColumn', 'tableRow', 'mergeTableCells' ]
                    }
                    ,
                }


const TitlexConfig = {   plugins: [Essentials, Alignment, Bold, Heading, Italic, PublishArticle ],
                        toolbar: [ 'heading', '|', 'alignment', 'bold', 'italic', 'publishArticle', 'undo', 'redo' ] }

const TitleConfig = {   plugins: [Essentials, Paragraph, PublishArticle ],
                        toolbar: [ 'publishArticle', 'undo', 'redo' ], }


export default class TextEditor extends React.Component {

    timer_id = null

    onChange = ( event, editor ) => {
        this.props.textasset.text = editor.getData()
        if (this.timer_id === null) {
            this.timer_id = setTimeout(() => {
                this.timer_id = null
                this.props.textasset.save()
            }, 2000)
        }
    }

    render() {
        return (
            <CKEditor
                editor={ InlineEditor }
                data={ (this.props.textasset && this.props.textasset.text) || (
                    this.props.children?((this.props.children.length>1 && typeof this.props.children !== 'string' )?
                        this.props.children.reduce( (s,i) => s+reactElementToJSXString(i),'')
                        : reactElementToJSXString(this.props.children)):''
                ) }
                config = { this.props.title?TitleConfig:TextConfig }
                onChange={ this.onChange }
                onInit={ editor => {
                    editor.textasset = this.props.textasset
                    if (this.props.title) {
                        editor.keystrokes.set( 'Enter', ( data, cancel ) => {
                            cancel();
                        } );
                        editor.keystrokes.set( 'Shift+Enter', ( data, cancel ) => {
                            cancel();
                        } );

                        editor.editing.view.document.on('enter', (event) => { event.stop() } )
                    }
                }

                }
            />
        )
    }
}
