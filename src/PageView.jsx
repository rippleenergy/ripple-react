import React from 'react'
import GA from 'react-ga'

export class PageView extends React.Component {
    componentDidUpdate(prevProps) {
        if (this.props.location.pathname !== prevProps.location.pathname) {
            GA.pageview(this.props.location.pathname)
        }
    }

    render() {
        return null
    }
}