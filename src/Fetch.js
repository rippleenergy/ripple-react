import "isomorphic-fetch"
import cookies from 'react-cookies'

/* makeURL('https://example.com', {foo: 1, bar: 'hi'}) -> 'foo=1&bar=hi'

Based on https://stackoverflow.com/questions/316781/how-to-build-query-string-with-javascript
*/
export const makeURL = (url, params) => {
    const qs = [];
    for (let p in params) {
        if (params.hasOwnProperty(p) && params[p] !== undefined) {
            qs.push(encodeURIComponent(p) + "=" + encodeURIComponent(params[p]))
        }
    }
    return `${url}${qs.length ? `?${qs.join('&')}` : ''}`
}

export const jsonRead = async (method, url, query, headers) => {

    const params = {
        method: method,
        headers: {
            "Accept": "application/json",
            ...headers,
        },
    }

    // TODO red - remove duplication
    try {
        const response = await fetch(makeURL(url, query), {
            ...params,
            credentials: 'same-origin', // 'include' <- CORS
        })

        /* 204 no response body */
        let data;
        let error;

        if (response.status !== 204) {
            /* 204 no response body */
            try {
                data = await response.json()
            } catch (jsonParseError) {
                // log the error but don't crash the request
                console.error(jsonParseError)
                error = jsonParseError

            }
        }
        return { response, data, error }

    } catch {
        //network error
        /**
         * return empty object so 
         * const {response, data, error} = await jsonPost
         * if(!reponse || error) {
         *     request has failed
         * }
         */
        return { error: e }
    }


}


export const jsonChange = async (method, url, query, headers, form) => {

    let body = null

    if (form) {
        const formData = new FormData()
        Object.keys(query).forEach(k => {
            formData.append(k, query[k])
        })
        body = formData
    }
    else {
        body = JSON.stringify(query)
    }

    const csrftoken = cookies.load('csrftoken')

    const params = {
        method: method,
        headers: {
            "Accept": "application/json",
            ...(csrftoken ? { "X-CSRFToken": csrftoken } : {}),
            ...headers,
        },
        body: body,
    }
    if (!form) params.headers["Content-Type"] = "application/json; charset=utf-8"

    try {
        const response = await fetch(url, {
            ...params,
            credentials: 'same-origin', // 'include' <- CORS
        })

        let data;
        let error;

        if (response.status !== 204) {
            /* 204 no response body */
            try {
                data = await response.json()
            } catch (jsonParseError) {
                // log the error but don't crash the request
                console.error(jsonParseError)
                error = jsonParseError

            }
        }
        return { response, data, error }

    } catch (e) {
        //network error
        /**
         * return empty object so 
         * const {response, data, error} = await jsonPost
         * if(!reponse || error) {
         *     request has failed
         * }
         */
        return { error: e }
    }

}

export const jsonGet = (url, query, headers) => {
    return jsonRead('GET', url, query, headers)
}

export const jsonPost = (url, query, headers, form = false) => {
    return jsonChange('POST', url, query, headers, form)
}

export const jsonPut = (url, query, headers, form = false) => {
    return jsonChange('PUT', url, query, headers, form)
}

export const jsonPatch = (url, query, headers, form = false) => {
    return jsonChange('PATCH', url, query, headers, form)
}

export const jsonDelete = (url, query, headers) => {
    return jsonChange('DELETE', url, query, headers)
}
