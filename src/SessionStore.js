import {jsonPatch} from './Fetch'
import cookie from "react-cookies"



export class SessionStore {

    constructor() {
        this.data = {}
    }

    async update(values) {

        try {
            const headers = {}
            const csrftoken = cookie.load('csrftoken')
            if (csrftoken) {
                headers["X-CSRFToken"] = csrftoken
            }

            const { response, data } = await jsonPatch('/api/update_session_data', values, headers)
            Object.assign(this.data, values)
            return true
        } catch (e) {
            console.log(`${this.constructor.name}: Error: ${toString(e)}`)
            return false
        }
    }

    initialize() {
    }

}




export const sessionStore = new SessionStore()
