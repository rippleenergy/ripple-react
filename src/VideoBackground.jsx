import React from 'react'
import VideoCover from './VideoCover'

export class VideoBackground extends React.Component {

state = {
    resizeNotifier: () => {},
  }

render() {
    const videoOptions = {
      muted: true,
      autoPlay: true,
      loop: true,
      playsInline: true,
    };
    const style = {
      width: '100vw',
      height: '620px',
      top: '-56px',
      position: 'absolute',
      zIndex: -1,
    };
return (
      <div style={style} >
        <VideoCover
          videoOptions={videoOptions}
          remeasureOnWindowResize
          src = {this.props.video}
          getResizeNotifier={resizeNotifier => {
            this.setState({
              resizeNotifier,
            });
          }}
        />
      </div>
    );
  }
}
export default VideoBackground;