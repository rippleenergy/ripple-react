import React from 'react'

export function Steps(props) {
    return(
        <div style={{display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems:'flex-end', width:'320px', paddingBottom: "20px"}}>
            { props.labels.map( (v,i,a) =>
                <div key={'step'+i.toString()}>
                    <div style={{textAlign: 'center'}}>{v}</div>
                    <div style={{display: 'flex', flexDirection: 'row', height: "40px"}}>
                        <div style={{padding:'0', margin:'0', height: "40px"}}><svg style={{width: "100%", height: "40px"}}>{i!==0 && <line x1="0" y1="50%" x2="100%" y2="50%" style={{stroke:"rgb(0,0,0)",strokeWidth:"2"}}/>}</svg></div>
                        <div style={{padding:'0', margin:'0', height: "40px"}}><svg style={{width: "30px", height: "40px"}}><circle  cx="50%" cy="50%" r="14px" style={{fill:props.select>=i+1?'black':'none',stroke:"rgb(0,0,0)",strokeWidth:"2"}}/></svg></div>
                        <div style={{padding:'0', margin:'0', height: "40px"}}><svg style={{width: "100%", height: "40px"}}>{i!==a.length-1&&<line x1="0" y1="50%" x2="100%" y2="50%" style={{stroke:"rgb(0,0,0)",strokeWidth:"2"}}/>}</svg></div>
                    </div>
                </div>
            ) }
        </div>

    )
}