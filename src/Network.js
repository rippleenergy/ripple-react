import cookie from "react-cookies"
import { jsonGet, jsonDelete, jsonPost, jsonPatch, jsonPut, makeURL } from "./Fetch"
import { observable, transaction, action, computed } from "mobx"


export class JsonRest {

    static cache = {}

    @observable data = null
    @observable error_data = null
    @observable status = ''
    @observable load_promise = null
    @observable ok = true

    static add_csrf(headers) {
        const csrftoken = cookie.load('csrftoken')
        let ret = headers || {}
        if (csrftoken) {
            ret["X-CSRFToken"] = csrftoken
        }
        return ret
    }

    method(m) {
        if (typeof (m) === 'string') {
            return {
                POST: jsonPost,
                GET: jsonGet,
                PUT: jsonPut,
                PATCH: jsonPatch,
                DELETE: jsonDelete,
            }[m]
        }
        return m
    }

    static getCacheKey(url, query, headers) {
        let h = makeURL(url, query) + this.name
        let hash = 0
        for (let i = 0; i < h.length; i++) {
            let char = h.charCodeAt(i)
            hash = ((hash << 5) - hash) + char
            hash = hash & hash
        }
        return hash
    }

    @computed
    get id() {
        return (this.data && this.data.id) || this.object_id || ''
    }

    get urlWithId() {
        if (this.base_url && this.id)
            return this.base_url.endsWith('/') ? `${this.base_url}${this.id}/` : `${this.base_url}/${this.id}/`
        return this.base_url
    }

    get_url() {
        return [this.exclude_id_on_get ? this.base_url : this.urlWithId, this.query, {}]
    }

    post_url() {
        return [this.urlWithId, {}]
    }

    put_url() {
        return [this.urlWithId, {}]
    }

    patch_url() {
        return [this.urlWithId, {}]
    }

    delete_url() {
        return [this.urlWithId, {}]
    }

    onWrite() {

    }

    constructor(base_url, pars = {}) {

        const hash = this.constructor.getCacheKey(...this.get_url())

        /*
        // disable cache for now
        if (base_url && this.constructor.cache[hash]) {
            const instance =  this.constructor.cache[hash]
            if (pars.load && this.load_promise === null) instance.get()
            return instance
        }*/

        this.base_url = base_url
        this.http_code = 0
        this.error_code = 0
        this.message = ''
        this.ok = true
        this.hash = hash
        this.loaded_at = null
        this.load_promise = null
        this.readonly = false
        this.object_id = pars.object_id || ''
        this.constructor.cache[hash] = this
        this.exclude_id_on_get = pars.exclude_id_on_get;
        this.query = pars.query

        if (pars.load) this.get()
    }

    @action
    processResponse(response, data) {

        this.loaded_at = Date.now()
        if(!response) {
            //no response from server
            this.ok = false
            this.status = 'error'
            return null
        }

        this.http_code = response.status
        this.message = response.statusText
        if (response.status >= 300 && response.status < 500) {
            this.error_code = this.http_code
            if (data) {
                this.error_data = data
                if (data.error_code) this.error_code = data.error_code
                if (data.message) this.message = data.message
            }
        }

        if (response.status >= 200 && response.status < 300) {
            this.ok = true
            this.status = 'ok'
            return data
        }
        else {
            this.ok = false
            this.status = 'error'
            return null
        }
    }

    @action
    async load(url, query = this.query, headers, reload = false) {

        if (!reload && this.ready) return this.data

        this.status = 'loading'
        try {
            if (this.load_promise == null) {
                this.load_promise = this.method("GET")(url, query, headers)
            }
            const { response, data, error } = await this.load_promise


            transaction(() => {
                this.data = data
                this.load_promise = null
            })

            return this.processResponse(response, data, error)
        }
        catch (e) {
            this.ok = false
            this.message = e.name + ': ' + e.message
            this.status = 'error'
            return null
        }
    }

    @computed
    get value() {
        if (this.ready) {
            return this.data
        }
        return null
    }

    @computed
    get ready() {
        return this.load_promise == null && this.status === 'ok' && this.ok && this.data != null
    }

    @computed
    get loading() {
        return this.load_promise != null
    }

    @action
    get(reload = false) {
        return this.load(...this.get_url(), reload)
    }

    @action
    async write(in_data, method, url, headers, { write = true } = {}) {
        method = this.method(method)
        this.status = 'saving'
        const { response, data } = await method(url, in_data || {}, url.startsWith('/') ? this.constructor.add_csrf(headers) : headers)
        const ret = this.processResponse(response, data)
        if (write) {
            await this.onWrite(ret)
        }
        return ret
    }

    @action
    async delete_(in_data, url, headers) {
        this.status = 'saving'
        try {
            const { response, data } = await this.method('DELETE')(url, in_data || {}, url.startsWith('/') ? this.constructor.add_csrf(headers) : headers)
            return this.processResponse(response, data)
        }
        catch (e) {
            this.ok = false
            this.message = e.name + ': ' + e.message
            this.status = 'error'
            return null
        }
    }

    @action
    delete() {
        return this.delete_({}, ...this.delete_url())
    }

    @action
    post(data, writeProps) {
        return this.write(data, 'POST', ...this.post_url(), writeProps)
    }

    @action
    patch(data) {
        return this.write(data, 'PATCH', ...this.patch_url())
    }

    @action
    put(data) {
        return this.write(data, 'PUT', ...this.put_url())
    }

}
