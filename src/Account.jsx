import { JsonRest } from "./Network"
import { history } from './History'
import { action, computed, observable, transaction } from "mobx"
import React from "react"

export class Account extends JsonRest {

    constructor(url, pars) {
        super(url || '/api/account/status', pars || { load: true })

        this.username_field = pars.username_field || 'username'
    }

    clientRoutes = {
        home: '/account',
        logIn: '/account/sign-in',
        signUp: '/account/sign-up',
        resetPassword: '/account/reset-password',
        resetPasswordConfirm: '/account/reset-password-confirm',
        anonymousHome: '/'
    }


    post_url() {
        return ['/api/account/log_in', {}]
    }

    @computed
    get isLoggedIn() {
        return !!(this.data && this.data.is_authenticated)
    }

    @computed
    get isAdmin() {
        return (this.data && this.data.is_admin)
    }

    @computed
    get isEditor() {
        return (this.data && this.data.is_admin)
    }

    @computed
    get isVerified() {
        return (this.data && this.data.is_email_verified)
    }

    @computed
    get values() {
        return
    }

    onWrite(response_data) {
        if (response_data) {
            this.data = response_data
        } else {
            if (this.ok) {
                return this.get(true)
            }
        }

    }

    @action
    login(username, password) {
        // change this to just set data to be the result of .post rather 
        // than re-get?
        return this.post({ [this.username_field]: username, password: password })
    }

    @action
    async logout(redirect, to = this.clientRoutes.anonymousHome) {
        // TODO red handle log_out errors
        // TODO red some components based on account.isLoggedIn will
        // re render before network calls complete
        // this causes inconsistent routing on redirect routes 
        await this.write({}, 'POST', '/api/account/log_out')
        if (redirect) {
            history.push(to)
        }
        if (!this.ok) this.data = {}
    }

}

export const AccountContext = React.createContext(new Account(null, { load: false }))