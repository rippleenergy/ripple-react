import React from 'react'
import { Form, Button} from "react-bootstrap"
import { FormComponent } from "../"
import { observable } from "mobx"
import { useLocalStore, useObserver} from "mobx-react-lite"
import { AccountContext } from "../Account"


class LoginFormHelper extends FormComponent {
    submit_url = '/api/login'
    dataManager = account

    constructor(props) {
        super(props, {
            submit_url: '/api/login',
            dataManager: account,
            next_url: props.next || '/'
        })

        history.replace('/login')
    }

    @observable badCredentials = false

    serverError(code, message) {
        if (code === 401 || code === "ERR_AUTH_FAIL") {
            this.badCredentials = true
            return true
        }
        return false
    }

    onChange(name, value) {
        this.badCredentials = false
        return {name, value}
    }

}

export function LoginForm(props) {

    const account = React.useContext(AccountContext)

    const helper = useLocalStore(() => new LoginFormHelper(props,
        {
            submit_url: '/api/login',
            dataManager: account,
            next_url: props.next || '/'
        })
    )

    return(useObserver( () =>
            <Form style={{maxWidth: "500px", width: "100%", alignItems: "center"}}
                  onSubmit={helper.submit}
            >
                    <Form.Group controlId="formBasicEmail">
                    <Form.Label>Email address</Form.Label>
                    <Form.Control type="email" placeholder="Enter email"
                                  name="username" onChange={helper.handleChange}
                                  required
                                  isInvalid={helper.badCredentials} />
                    </Form.Group>

                    <Form.Group controlId="formBasicPassword">
                    <Form.Label>Password</Form.Label>
                    <Form.Control type="password" placeholder="Password"
                                  name="password" onChange={helper.handleChange}
                                  required
                                  isInvalid={helper.badCredentials}
                    />
                    <Form.Control.Feedback type="invalid">
                        Sorry, this email and password combination is not recognised.
                    </Form.Control.Feedback>
                    </Form.Group>
                <div style={{textAlign : "center"}}>
                    <Button type="submit">
                        Sign in
                    </Button>
                </div>
            </Form>
    ))
}