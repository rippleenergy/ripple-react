import React from 'react'
import { Form, Button, ProgressBar} from "react-bootstrap"
import {useObserver, useLocalStore} from "mobx-react-lite"
import { observable } from "mobx"

import { FormComponent } from "../FormComponent"

class ChangePasswordHelper extends FormComponent {

    @observable passwordStrength = 0
    @observable userExists = false
    existingUsers = new Set()

    static analyzePassword(pw) {

        let strongRegex = new RegExp("^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})");
        let mediumRegex = new RegExp("^(((?=.*[a-z])(?=.*[A-Z]))|((?=.*[a-z])(?=.*[0-9]))|((?=.*[A-Z])(?=.*[0-9])))(?=.{6,})");
        let ret = 0
        if(strongRegex.test(pw)) {

            ret = 90
        } else if(mediumRegex.test(pw)) {
            ret = 70
        } else {
            ret = 20
        }
        return ret
    }

    constructor(props, conf) {
        super(props, conf)
    }


    mapValues() {
       return {email: this.values.email, password: this.values.password1 }
    }

    onChange(name, value) {
        if (name === "password1") {
            this.passwordStrength = this.constructor.analyzePassword(value)
        }
        if (name === "email") {
            this.userExists = this.existingUsers.has(value)
        }
        return {name, value}
    }

    serverError(code, message) {
        if (code === "ERR_USER_EXISTS") {
            this.userExists = true
            this.existingUsers.add(this.values.email)
            return true
        }
        return false
    }
}

export function SignUpForm(props) {

    const helper = useLocalStore(() => new ChangePasswordHelper(props,
        {
            next_url:  '/subscribe-mysub',
            submit_url: '/api/hl/signup'
        })
    )

    const boxStyle={height:"120px", width:"300px", border: "solid 1px black", borderRadius: "10px", padding: "10px", margin: "10px", display: "flex", flexDirection:"column", justifyContent : "center", alignItems: "center"}

    return(useObserver(() =>
            <Form style={{maxWidth: "500px", width: "100%", alignItems: "center"}}
                  onSubmit={helper.submit}
            >
                <Form.Group controlId="formBasicEmail">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email" placeholder="Enter email" name="email" onChange={  helper.handleChange } isInvalid={helper.userExists}/>
                <Form.Control.Feedback type="invalid">
                    We already have an account with this email.
                </Form.Control.Feedback>
                <Form.Text className="text-muted">
                    We'll never share your email with anyone else.
                </Form.Text>
                </Form.Group>

                <Form.Group controlId="formBasicPassword">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password" placeholder="Password" name="password1" required onChange={helper.handleChange} />
                </Form.Group>

                <Form.Group controlId="formBasicPassword2">
                <Form.Label>Please re-enter password</Form.Label>
                <Form.Control type="password" placeholder="Password"
                              name="password2" onChange={helper.handleChange}
                              isInvalid={helper.values.password1!==helper.values.password2} />

                <Form.Control.Feedback type="invalid">
                  {helper.values.password1!==helper.values.password2 && "The passwords are different"}
                </Form.Control.Feedback>
                </Form.Group>
                <h3>Password strength:</h3>
                <ProgressBar variant={helper.passwordStrength>80?"success":(helper.passwordStrength>60?"warning":"danger")} now={helper.passwordStrength} />
                <div style={{width: "100%", display: "flex", justifyContent: "center", paddingBottom: "20px"}}>
                    <div style={boxStyle}>
                        <h3>Minimum of 8 characters</h3>
                        <h3>1 letter and 1 number</h3>
                    </div>
                </div>
                <div style={{textAlign : "center"}}>
                    <Button type="submit">
                        Add my details
                    </Button>
                </div>
            </Form>
    ))
}