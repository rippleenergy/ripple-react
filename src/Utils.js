/*********************************************************************/
/* Logging
/*********************************************************************/

/* Force a MobX value to a javascript object */
export const asData = x => {
    if (x == null)
        return null;
    return JSON.parse(JSON.stringify(x))
}

export const toString = (obj) => {
    return JSON.stringify(obj)
}

/* Log mobx values (dump to JSON) */
export const log = (...args) => {
    console.log(...args.map(asData))
}

/* Create a logger that logs the filename and arguments:

    log = logger('/path/to/file.js')
    log(...)
*/
export const logger = (fileName) => {
    return (...args) => {
        log(fileName, ...args)
    }
}


/*********************************************************************/
/* Basics
/*********************************************************************/

export const min = (x, y) => x <= y ? x : y;
export const max = (x, y) => x >= y ? x : y;

/*********************************************************************/
/* Lists
/*********************************************************************/

export const range = (stop) => {
    const result = [];
    for (var i = 0; i < stop; i++) {
        result.push(i)
    }
    return result
}

export const zip = (xs, ys) => {
    return range(max(xs.length, ys.length)).map(i => {
        return [xs[i], ys[i]]
    })
}

export const slice = (xs, start, stop, step) => {
    const result = [];
    for (var i = start; i < stop; i += step) {
        result.push(xs[i])
    }
    return result
}

export const pairs = (xs) => {
    const fsts = slice(xs, 0, xs.length, 2);
    const scnds = slice(xs, 1, xs.length, 2);
    return zip(fsts, scnds)
}

/*********************************************************************/
/* Types
/*********************************************************************/

export const isNumber = (x : String) => {
    const n = parseInt(x);
    return n === n /* n is not NaN */
}

/*********************************************************************/
/* URIs
/*********************************************************************/

/* makeURL('https://example.com', {foo: 1, bar: 'hi'}) -> 'foo=1&bar=hi'

Based on https://stackoverflow.com/questions/316781/how-to-build-query-string-with-javascript
*/
export const makeURL = (url, params) => {
    const qs = [];
    for (let p in params) {
        if (params.hasOwnProperty(p) && params[p] !== undefined) {
            qs.push(encodeURIComponent(p) + "=" + encodeURIComponent(params[p]))
        }
    }
    return `${url}?${qs.join('&')}`
}
