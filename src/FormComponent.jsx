import React from 'react'
import {history } from "./History"
import { observable } from "mobx"
import { JsonRest } from "./Network"

class FormJsonRest extends JsonRest {

        constructor(load_url, submit_url, pars = {}) {
            super(load_url, pars )
            this.submit_url = submit_url
        }

        post_url() {
            if (this.submit_url)
                return [this.submit_url,{}]

            return super.post_url()
        }
}

export class FormComponent {

    @observable values = {}

    constructor(props, conf = {}) {
        this.props = props
        this.load_url = props.load_url || conf.load_url
        this.submit_url = props.submit_url || conf.submit_url
        this.next_url = props.next_url || conf.next_url
        this.object_id = props.object_id || conf.object_id || ''
        this.dataManager = props.dataManager || conf.dataManager
            || new FormJsonRest(this.load_url, this.submit_url, {object_id: this.object_id })

        if (this.load_url) this.load()
    }


    mapValues() {
        return this.values
    }

    serverError(code, message, values) {
        return false
    }

    onSubmit(e, values) {
        return false
    }

    onSuccess(values) {
    }

    async update() {

        let values = this.mapValues()

        if (!this.submit_url) {
            return false
        }

        await this.dataManager.post(values)
        if (!this.dataManager.ok) this.serverError(this.dataManager.error_code, this.dataManager.message, values)
        return this.dataManager.ok
    }

    async load() {
        if (!this.load_url) return
        const values = await this.dataManager.get(true)

        if (this.dataManager.ok) {
            this.values = values
            return true
        }
        return false
    }


    onChange (name, value) {
        return {name, value}
    }

    handleChange = (e) => {

        let n = e.target.name
        let v
        if (e.target.type === 'checkbox' ) {
            v = e.target.checked
        }
        else {
            v =  e.target.value
        }
        let { name, value } =  this.onChange(n, v)
        this.values[name]=value
    }


    submit = async (e) => {
        if (this.onSubmit(e, this.values)) return
        e.preventDefault()
        e.stopPropagation()
        if (await this.update()) {
            this.onSuccess(this.values)
            if (this.next_url) {
                history.push(this.next_url)
            }
            else {
                // console.log(`${this.constructor.name} saved`)
            }
        }
    }

}