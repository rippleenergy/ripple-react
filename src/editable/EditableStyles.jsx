import styled from "styled-components"

export const EditableBox = styled.div`
    border-style: solid;
    border-width: ${props => props.editor?'2px':(props.edited?'1px':'0')};
    padding: ${props => {
        let p = 3
        if (props.editor) p-=3
        else if (props.edited) p-=1
        return p +'px'    
    }};    
    border-color: ${props => props.editor?'blue':'rgba(255,0,0,0.25)'};
    position: relative; 
    ${props => {
        if (!props.admin || props.editor ) return ''
        let p = 1
        return ':hover { border-width: 2px; border-color: ' + (props.editor?'blue':'red') + '; padding: ' + p + 'px'
    }}
`
export const EditButton = styled.div`
    position: absolute;
    width: 100%;
    left:-10px;
    top:-10px;
    display: none;
    
    ${EditableBox}:hover & {
        display: block;
    }
`