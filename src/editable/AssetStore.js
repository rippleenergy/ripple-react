import React from 'react'
import { logger } from '../Utils'
import { observable, transaction, computed} from 'mobx'
import cookie from "react-cookies"
import { jsonGet, jsonPatch, jsonPost, jsonDelete} from "../Fetch"

const log = logger('ArticleStore.js')

/** @namespace GlobalAssets  **/

class AssetBase {

    @observable status = ''
    @observable id = null
    @observable published = null
    form_save = false

    assetManager = null

    assetid = null

    url = null
    value = {}

    promise_load = null

    constructor(assetManager, {assetid=null, value=null}) {
        this.status = ''
        this.assetManager = assetManager
        if (value) {
            this.value = value
            this.id = value.id
            this.assetid = value.assetid
            this.published = value.published
            this.depatch(value)
            this.status = 'ready'
        }
        else {
            this.assetid = assetid
            this.value.published = false
            this.default()
        }
    }

    default() {
    }

    patch(value) {
        return value
    }

    depatch(value) {
        return null
    }

    postSave(data) {
    }

    async _load() {
        try {
            const { response, data }  = await jsonGet(this.url, { assetid: this.assetid }, {})
            transaction( () => {
                    this.response = response
                    if (data.length>0) {
                        this.value = data[0]
                        this.depatch(this.value)
                        this.status = 'ready'
                    } else {
                        this.status=''
                        this.id=null
                        this.value={}
                        this.published=null
                        this.default()
                    }
                })

        } catch (e) {
            log(`${this.constructor.name}: Error: ${toString(e)}`)
            this.status = 'error'
        }
    }

    async load() {
        if (this.promise_load === null) {
            this.promise_load = this._load()
        }
        await this.promise_load
    }


    async save() {
        try {
            const headers = {}
            const csrftoken = cookie.load('csrftoken')
            if (csrftoken) {
                headers["X-CSRFToken"] = csrftoken
            }

            if (this.id && !this.value.published) {
                const { response, data } = await jsonPatch(`${this.url}/${this.id}/`, this.patch({}), headers, this.form_save)
                this.response = response
                this.postSave(data)
            }
            else {
                this.value.published = false
                delete this.value.id
                this.value = this.patch(this.value)
                let _value = { assetid: this.assetid, owner: assetManager.userid,
                              module: assetManager.module, ...this.value}
                const { response, data } = await jsonPost(`${this.url}/`, _value, headers, this.form_save)
                this.response = response
                this.id = data.id
                this.published = false
                this.value.id = this.id
                this.postSave(data)
            }

        } catch (e) {
            log(`${this.constructor.name}: Error: ${toString(e)}`)
            this.status= 'error'
        }
    }

    async publish() {
        try {
            const headers = {}
            const csrftoken = cookie.load('csrftoken')
            if (csrftoken) {
                headers["X-CSRFToken"] = csrftoken
            }

            if (this.id) {
                await jsonPatch(`${this.url}/${this.id}/`, {published: true}, headers)
                this.published = true
            }
            else {
            }

        } catch (e) {
            log(`${this.constructor.name}: Error: ${toString(e)}`)
            this.status= 'error'
        }
    }

    async reset() {
        try {
            const headers = {}
            const csrftoken = cookie.load('csrftoken')
            if (csrftoken) {
                headers["X-CSRFToken"] = csrftoken
            }

            if (this.id) {
                await jsonDelete(`${this.url}/${this.id}/`, {}, headers)
                await this._load()
            }
            else {
            }

        } catch (e) {
            log(`${this.constructor.name}: Error: ${toString(e)}`)
            this.status= 'error'
        }
    }

}


class ImageAsset extends AssetBase {
    @observable image
    form_save = true

    url = "/api/imageassets"

    constructor(assetManager, {assetid=null, value=null}) {
        super(assetManager, {assetid, value})
    }

    patch(value) {
        value.image = this.image
        return value
    }

    depatch(value) {
        this.image = value.image
    }

    default() {
        this.image = null
    }

    postSave(data) {
        this.image = data.image
    }

}


class RichTextAsset extends AssetBase {
    @observable text

    url = "/api/richtextassets"

    constructor(assetManager, {assetid=null, value=null}) {
        super(assetManager, {assetid, value})
    }

    patch(value) {
        value.text = this.text
        return value
    }

    depatch(value) {
        this.text = value.text
    }
}


class TextAsset extends RichTextAsset {

    @observable text

    url = "/api/textassets"

    constructor(assetManager, {assetid=null, value=null}) {
        super(assetManager, {assetid, value})
    }


    patch(value) {
        value.text = this.text
        return value
    }

    depatch(value) {
        this.text = value.text
    }

    default() {
        this.text = ''
    }
}


class ArticleAsset extends AssetBase {

    url = "/api/articles"

    constructor(assetManager, {assetid=null, value=null}) {
        super(assetManager, {assetid, value})
    }

    default() {
        this.text_object = this.assetManager.getRichText(this.assetid+'.text')
        this.image_object = this.assetManager.getImage(this.assetid+'.image')
        this.title_object = this.assetManager.getText(this.assetid+'.title')
    }

    depatch(value) {
        this.text_object = this.assetManager.getRichText(value.text.assetid)
        this.image_object = this.assetManager.getImage(value.image.assetid)
        this.title_object = this.assetManager.getText(value.title.assetid)
    }

    @computed
    get title() {
        return this.title_object?this.title_object.text:null
    }

    @computed
    get text() {
        return this.text_object?this.text_object.text:null
    }

    @computed
    get image() {
        return this.image_object?this.image_object.image:null
    }

    set title(title) {
        this.title_object.text = title
    }

    set text(text) {
        this.text_object.text = text
    }

    set image(image) {
        this.image_object.image = image
    }

    async save() {
        return await Promise.all([
            super.save,
            this.image_object.save,
            this.title_object.save,
            this.text_object.save ])
    }
}

class ArticleCollectionAsset extends AssetBase {


    url = "/api/article_collections"

    constructor(assetManager, {assetid=null, value=null}) {
        super(assetManager, {assetid, value})
    }

    depatch(value) {
        this.items = []
        value.items.forEach( (i) => {
            if (i.type === 'article') {
                this.items.push(this.assetManager.getArticle(i.assetid))
            } else if (i.type === 'article_collection') {
                this.items.push(this.assetManager.getArticleCollection(i.assetid))
            }
        })

    }

}


class AssetManager {

    richtext_assets = {}
    text_assets = {}
    image_assets = {}
    articles = {}
    article_collections = {}

    module = null
    userid = null
    is_admin = null
    version = null


    initialize() {

        const { module, version, is_admin, userid} = GlobalAssets
        this.module = module
        this.userid = userid
        this.is_admin = is_admin
        this.version = version

    }

    getObject(assetid, object_class, assets,  list) {
        if (! (assetid in assets) ) {
            const item = list.find((a) => a.assetid === assetid)
            if (item) {
                assets[assetid] = new object_class(this, {value: item})
            }
            else {
                assets[assetid] = new object_class(this, {assetid: assetid})
            }
        }
        return assets[assetid]

    }

    getArticle(assetid) {
       return this.getObject(assetid, ArticleAsset, this.articles, GlobalAssets.articles)
    }

    getRichText(assetid) {
        return this.getObject(assetid, RichTextAsset, this.richtext_assets, GlobalAssets.richtext)
    }

    getImage(assetid) {
        return this.getObject(assetid, ImageAsset, this.image_assets, GlobalAssets.images)
    }

    getText(assetid) {
        return this.getObject(assetid, TextAsset, this.text_assets, GlobalAssets.text)
    }

    getArticleCollection(assetid) {
       return this.getObject(assetid, ArticleCollectionAsset, this.article_collections, GlobalAssets.article_collections)
    }

}

export const assetManager = new AssetManager()