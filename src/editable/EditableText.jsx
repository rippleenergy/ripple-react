import React, { Suspense } from "react"
import { assetManager} from './AssetStore'
import { logger } from '../Utils'
import {Button, Dropdown, DropdownButton, Image, Modal} from 'react-bootstrap'
import { EditableBox, EditButton } from  './EditableStyles'
import {AccountContext} from "../Account"
import {useObserver} from "mobx-react-lite"

const log = logger('TextView.jsx')

function StaticText(props) {
    return (
        ( props.textasset && props.textasset.text )
        ?
        <div dangerouslySetInnerHTML={{ __html: props.textasset.text }} />
        :
        <div>{props.children}</div>
    );
}

const TextView = React.lazy(() => import('../editor/TextEditor'))

export function EditableText(props) {

    const [edit, setEdit] = React.useState(false)
    const [showEditMore, setShowEditMore] = React.useState(false)
    const accountContext = React.useContext(AccountContext)

    let textasset = null
    if (props.assetid) {
        textasset = props.title ? assetManager.getText(props.assetid) : assetManager.getRichText(props.assetid)
    }
    else if (props.textasset) {
        textasset = props.textasset
    }

    const EditMore = () => {
        return (
            <Modal show={showEditMore} style={{fontColor: "black"}} onHide={() => setShowEditMore(false)}>
            <Modal.Header closeButton>
                <Modal.Title>Text Edit</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                More edit options here....
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => setShowEditMore(false)}>
                    Close
                </Button>
                <Button variant="primary" disabled={false} >
                    Save Changes
                </Button>
            </Modal.Footer>
        </Modal>
     )}

    const DropMenu = () => {
        return (
            <EditButton>
                <EditMore/>
                <DropdownButton id="dropdown-basic-button" size="sm" variant="danger" style={{position: "absolute",  right:-10, top:-10}}title="Text">
                    <Dropdown.Item as="button" onClick={ () => textasset.publish() }>Publish</Dropdown.Item>
                    <Dropdown.Item as="button" onClick={ () => setEdit(!edit)}>{edit?'Close':'Edit'}</Dropdown.Item>
                    <Dropdown.Item as="button" onClick={ () => textasset.reset() }>Revert</Dropdown.Item>
                    <Dropdown.Item as="button" onClick={ () => textasset.reset() }>Reset</Dropdown.Item>
                    <Dropdown.Item as="button" onClick={ () => setShowEditMore(true)}>More...</Dropdown.Item>
                </DropdownButton>
            </EditButton>
        )
    }

    return ( useObserver(() =>
        <Suspense fallback={<StaticText textasset={textasset}>{props.children}</StaticText>} >
            {
                <EditableBox admin={accountContext.isEditor} editor={edit} edited={ textasset && !textasset.published && textasset.id}>
                    {accountContext.isEditor && <DropMenu/> }
                    {edit ?
                        <TextView textasset={textasset} title={!!props.title}>
                            {props.children}
                        </TextView>
                        :
                        <StaticText textasset={textasset} title={!!props.title}>
                            {props.children}
                        </StaticText>
                    }
                </EditableBox>
            }
        </Suspense>
    ))
}
