import React from 'react'
import {Image, Popover, OverlayTrigger, DropdownButton, Dropdown, Button, Modal} from "react-bootstrap"
import { assetManager } from "./AssetStore"
import { EditableBox, EditButton } from  './EditableStyles'
import {useObserver} from "mobx-react-lite"
import {AccountContext} from "../Account"


export function EditableImage(props) {

    let imageasset = props.assetid ? assetManager.getImage(props.assetid) : props.imageasset

    const accountContext = React.useContext(AccountContext)
    const [image_preview_url, setImagePreviewUrl] = React.useState(null)
    const [showUpload, setShowUpload] = React.useState(false)

    const {children, src, ...other_props } = props

    const ImageUpload = () => {
        const [file, setFile] = React.useState(null)
        const [preview, setPreview] = React.useState(null)
        return (
            <Modal show={showUpload} style={{fontColor: "black"}} onHide={() => setShowUpload(false)}>
            <Modal.Header closeButton>
                <Modal.Title>Upload new image</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <input type='file' onChange={(e) => {
                    setFile(e.target.files[0])
                    setPreview(URL.createObjectURL(e.target.files[0]))
                }}/>
                <p>Preview:</p>
                <div style={{width: '200px', height:'150p'}}>
                    {preview && <Image src={preview} fluid />}
                </div>
            </Modal.Body>
            <Modal.Footer>
                <Button variant="secondary" onClick={() => setShowUpload(false)}>
                    Close
                </Button>
                <Button variant="primary" disabled={!file} onClick={() => {
                    setImagePreviewUrl(URL.createObjectURL(file))
                    imageasset.image = file
                    imageasset.save()
                    setShowUpload(false)
                    setImagePreviewUrl(null)
                }}>
                    Save Changes
                </Button>
            </Modal.Footer>
        </Modal>
     )}

    function DropMenu(props) {
        return (
            <EditButton>
                <ImageUpload />
                <DropdownButton id="dropdown-basic-button" size="sm" variant="danger" style={{position: "absolute",  right:-10, top:-10}}title="Image">
                    <Dropdown.Item as="button" onClick={ () => imageasset.publish() }>Publish</Dropdown.Item>
                    <Dropdown.Item as="button" onClick={ () => imageasset.reset() }>Reset</Dropdown.Item>
                    <Dropdown.Item as="button" onClick={ () => setShowUpload(true)}>Upload...</Dropdown.Item>
                </DropdownButton>
            </EditButton>
        )
    }

    return (useObserver(() =>
        accountContext.isEditor?
            <EditableBox admin={accountContext.isEditor} edited={imageasset && !imageasset.published && imageasset.id}>
                {accountContext.isEditor && <DropMenu imageasset={imageasset} /> }
                <Image { ...other_props  } src={image_preview_url || (imageasset && imageasset.image) || src } >
                    { children }
                </Image>
            </EditableBox>
         :
            <Image { ...other_props } src={(imageasset && imageasset.image) || src} >
                { children }
            </Image>
    ))
}