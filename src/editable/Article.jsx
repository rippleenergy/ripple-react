import { useObserver } from "mobx-react-lite"
import React from "react"
import { assetManager } from "./AssetStore"
import { EditableText } from "./EditableText"
import { Col, Container, Row, Accordion, Card, Button } from "react-bootstrap"
import { EditableImage } from "./EditableImage"
import { ConfigContext } from "../ConfigContext"
import { AccountContext } from "../Account"

class ArticleLayoutFactory {

    constructor() {
        this.layouts = {}
    }

    getLayout(name) {
        return this.layouts[name]
    }

    addLayout(layout, name = null) {
        if (name === null) name = layout.name
        this.layouts[name] = layout
    }
}

const articleLayoutFactory = new ArticleLayoutFactory()

export function articleLayout(target, name = null) {
    articleLayoutFactory.addLayout(target, name)
    return target
}

articleLayout(PicTitleArticle, "PicTitleArticle")
export function PicTitleArticle(props) {

    const background = props.background || props.left ? "white" : "#EDFAF7"
    const article = (props.article) ? props.article : assetManager.getArticle(props.assetid)

    return (useObserver(() =>
        <Row style={{ backgroundColor: background }}>
            <Container >
                <Row>
                    <Col sm={12} md={{ span: 6, order: props.left ? 2 : 0 }} >
                        <h2 style={{ paddingTop: "15px" }}>
                            <EditableText textasset={article.title_object} title>
                                {props.title}
                            </EditableText>
                        </h2>
                        <EditableText textasset={article.text_object}>
                            {props.children}
                        </EditableText>
                        {props.extra}
                    </Col>
                    <Col sm={12} md={6}>
                        {
                            props.image_override || (
                            <div style={{padding: "15px", paddingLeft: 0, paddingRight: 0}}>
                                <EditableImage imageasset={article.image_object} src={props.image_url} fluid />
                            </div>
                            )
                        }
                    </Col>
                </Row>
            </Container>
        </Row>
    ))
}

articleLayout(TextOnlyArticle, "TextOnlyArticle")
export function TextOnlyArticle(props) {
    return (useObserver(() =>
        <EditableText textasset={props.article} title={!!props.title} >
            {props.children}
        </EditableText>
    ))
}

articleLayout(ColumnArticle, "ColumnArticle")
export function ColumnArticle(props) {
    const configContext = React.useContext(ConfigContext)

    return (useObserver(() =>
        <React.Fragment>
            {(!props.no_image) &&
                <EditableImage style={props.image_style || { width: "140px", height: "140px", padding: "10px" }}
                    imageasset={props.article.image} src={props.image_url} alt={props.title || ""} fluid />}
            {(!props.no_title) &&
                <h3 style={{ color: props.title_color || configContext.primaryColor, marginTop: "5px" }}>
                    <EditableText textasset={props.article.title_object} title>
                        {props.title}
                    </EditableText>
                </h3>}
            {(!props.no_text) &&
                <EditableText textasset={props.article.text_object}>
                    {props.children}
                </EditableText>}
        </React.Fragment>
    ))
}

articleLayout(TitleTextArticle, "TitleTextArticle")
export function TitleTextArticle(props) {
    const configContext = React.useContext(ConfigContext)

    return (useObserver(() =>
        <React.Fragment>
            {
                React.createElement(props.title_tag || "h2", { style: { color: props.title_color || configContext.primaryColor, paddingTop: "10px" } },
                    <EditableText textasset={props.article.title_object} title>
                        {props.title}
                    </EditableText>)
            }
            <EditableText textasset={props.article.text_object}>
                {props.children}
            </EditableText>
        </React.Fragment>
    ))
}


articleLayout(TitleTextAccordionArticle, "TitleTextAccordionArticle")
export function TitleTextAccordionArticle(props) {
    const configContext = React.useContext(ConfigContext)

    return (useObserver(() =>
        <Accordion>
            <Accordion.Toggle as="div" variant="link" eventKey="0" style={{cursor: "pointer"}}>
                {
                    React.createElement(props.title_tag || "h2", { style: { color: configContext.primaryColor, paddingTop: "10px" } },
                        <EditableText textasset={props.article.title_object} title>
                            {props.title}
                        </EditableText>)
                }
            </Accordion.Toggle>
            <Accordion.Collapse eventKey="0">
                <EditableText textasset={props.article.text_object}>
                    {props.children}
                </EditableText>
            </Accordion.Collapse>
        </Accordion>
    ))
}




export function Article(props) {

    const article = props.assetid ? assetManager.getArticle(props.assetid) : props.article
    const layout = (typeof props.layout === 'string') ? articleLayoutFactory.getLayout(props.layout) :
        (props.layout || TextOnlyArticle)

    return (useObserver(() =>
        React.createElement(layout, { article: article, ...props }, props.children)
    ))
}


export function ArticleList(props) {

    const accountContext = React.useContext(AccountContext)
    const articleCollection = assetManager.getArticleCollection(props.id)

    return (useObserver(() =>
        <>
            {
                (articleCollection && articleCollection.items)
                    ?
                    articleCollection.items.map((a, i) =>
                        <Article layout="PicTitleArticle" article={a} key={i} left={i % 2} />
                    )
                    : (accountContext.isEditor?<Button size="sm" variant="danger">+</Button>:'')
            }
        </>
    ))
}
