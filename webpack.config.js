const path = require('path');
const { styles } = require( '@ckeditor/ckeditor5-dev-utils' );

module.exports = env => {

    let production = Boolean(env && ('production' in env) && env.production);
    let watch = Boolean(env && ('watch' in env) && env.watch);
    let dist_dir = (env && ('dist_dir' in env) && env.dist_dir) || './dist';

    let conf = {
        context: __dirname,
        watch: watch,
        mode: production?'production':'development',
        target: 'web',

        entry: {
            index: './src/index.js',
            editor: './src/editor/index.js',
        },
        output: {
            path: path.resolve(__dirname, dist_dir),
            filename: "[name].js",
            libraryTarget: 'umd',
            // libraryExport: 'default',
            library: 'ripple-react',
            //umdNamedDefine: true
        },
        externals:[
            'react',
            'react-dom',
            'lodash',
            'mobx',
            'mobx-react',
            'react-bootstrap',
            'react-ga',
            'react-cookies',
            'react-loader',
            'styled-components'
        ],
    
        module: {
            rules: [
                {
                    // Or /ckeditor5-[^/]+\/theme\/icons\/[^/]+\.svg$/ if you want to limit this loader
                    // to CKEditor 5 icons only.
                    test: /\.svg$/,
                    use: [ 'raw-loader' ]
                },
                {
                    test: /\.js.?$/,
                    exclude: /node_modules\/(?!(@restart)\/).*/,

                    use: {
                        loader: 'babel-loader',
                        options: {
                            plugins: [
                                        ["@babel/plugin-proposal-decorators",{"legacy": true}],
                                        ["@babel/plugin-proposal-class-properties", {"loose": true}],
                                        'babel-plugin-styled-components',
                                        "@babel/plugin-syntax-dynamic-import",
                                        ["module:babel-root-slash-import", {"rootPathSuffix": "src"}],
                            ],
                            presets: [['@babel/preset-env', {"modules":false, "debug":false}],"@babel/preset-flow",'@babel/preset-react']
                        }
                    }
                },
                {test: /\.css$/, use: [
                        'style-loader',
                        'css-loader',
                        {
                            loader: 'postcss-loader',
                            options: styles.getPostCssConfig( {
                                themeImporter: {
                                    themePath: require.resolve( '@ckeditor/ckeditor5-theme-lark' )
                                },
                                minify: true
                            } )
                        }
                    ]

                },
                {
                    test: /\.(scss)$/,
                    use: [
                        { loader: 'style-loader' }, // inject CSS to page
                        { loader: 'css-loader' }, // translates CSS into CommonJS modules
                        { loader: 'sass-loader'}, // compiles Sass to CSS
                    ]
                }
            ],
        },
        resolve: {
            modules: ['node_modules',],
            extensions: ['.js', '.jsx'],
            symlinks: false
        },
    };

    return conf;
};
