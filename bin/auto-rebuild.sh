#!/bin/bash
# https://stackoverflow.com/questions/8699293/how-to-monitor-a-complete-directory-tree-for-changes-in-linux

sudo bash -c 'echo 16384 >> /proc/sys/fs/inotify/max_user_watches'

while true; do
    inotifywait -e modify,create,delete -r ./src && \
    ./rebuild.sh
done
